'use strict';

const fs = require("fs");
const path = require("path");
const mkdirp = require('mkdirp');
const util = require('./util.js');

const mod_dir = util.getBaseDir(),
			name 		= util.getModuleName();

(async function initModule() {
	// let git_url = await git('config --get remote.origin.url');
	// git_url = git_url.stdout;
	// git_url = "https://" + git_url.split("@").pop().replace(/:/, "/").split(".").slice(0,-1).join(".");
	const git_url = 'https://gitlab.com/moerills-fvtt-modules/' + path.basename(mod_dir),
				manifestUrl = git_url + '/raw/master/module.json',
				zipUrl = git_url + '/dist/' + path.basename(mod_dir) + '.zip';

	const metadata = {
		name: path.basename(mod_dir),
		title: name,
		description: "Module Description",
		version: "0.0",
		author: "Moerill",
		systems: [],
		scripts: [],
		styles: [],
		packs: [],
		languages: [
			{
				"lang": "en",
				"name": "English",
				"path": "lang/en.json"
			}
		],
		url: git_url,
		manifest: manifestUrl,
		download: zipUrl
	};
	fs.writeFileSync(path.join(mod_dir, "module.json"), JSON.stringify(metadata, null, 2));
	mkdirp.sync(path.join(mod_dir, "lang"));
	fs.writeFileSync(path.join(mod_dir, "lang/en.json"), JSON.stringify({"MODULE.STRING_KEY": "Translation"}, null, 2));
	mkdirp.sync(path.join(mod_dir, "dist"));
	mkdirp.sync(path.join(mod_dir, "doc"));

	let readme = `
# ${name}  
[![MIT-License](https://img.shields.io/badge/License-MIT-black?style=flat-square)](${git_url+'/raw/master/LICENSE'}) 
[![PayPal](https://img.shields.io/badge/Buy%20Me%20A%20Coffee-PayPal-blue?style=flat-square)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FYZ294SP2JBGS&source=url)  
## Installation
* Go to the *Add-On Modules* tab in FVTTs setup screen
* Press *Install Module*
* Paste \`\`${manifestUrl}\`\` into the text field
* Press *Install*

# License
This work is licensed under the MIT License and Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).
	`;
	fs.writeFileSync(path.join(mod_dir, 'README.md'), readme);
})();