# Token Mold

[![PayPal](https://img.shields.io/badge/Buy%20Me%20A%20Coffee-PayPal-blue?style=flat-square)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FYZ294SP2JBGS&source=url)  

## This Repository is now deprecated, but the mod lives on on GitHub: https://github.com/Moerill/token-mold#token-mold !  
Please keep from creating issues or suggestions here, but move them over to GitHub, See here for instructions : https://github.com/Moerill/token-mold/#bug-reporting.
